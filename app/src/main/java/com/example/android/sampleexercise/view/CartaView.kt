package com.example.android.sampleexercise.view

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import android.view.View

class CartaView : View {

    private val paint = Paint(Paint.ANTI_ALIAS_FLAG)

    private var borderWidth = 4.0f

    private var size = 320

    private var listener: OnFaceClickListener? = null

    constructor(context: Context) : super(context) {
        //Crear una nueva instancia desde codigo Kotlin
    }


    constructor(context: Context, attrs: AttributeSet) : super(context, attrs){
        // Crear una nueva instancia a partir de XML
        // val a : TypedArray = context.theme.obtainStyledAttributes(attrs, R.styleable.CartaView, 0, 0)
        // Log.i(javaClass.name, String.format("Alto Boca: %f" , altoBoca ))
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr){
        // Crear una nueva instancia a partir de XML con un estilo de un atributo de theme.
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        // Para definir que el ancho y el alto se recalculen ante cualquier cambio
        // en el viewport (sea responsive).
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        val width : Int = View.MeasureSpec.getSize(widthMeasureSpec)
        //   val modeWidth : Int = View.MeasureSpec.getMode(widthMeasureSpec)
        val height : Int = View.MeasureSpec.getSize(heightMeasureSpec)
        size = Math.min(width, height)
        setMeasuredDimension(size, size)
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        paint.color = Color.MAGENTA
        paint.style = Paint.Style.FILL

        val mouthRect = RectF(size*2+ 0f,
            size + 0f,
            size*2 + 0f,
            size + 0f)
        canvas?.drawRect(mouthRect, paint)
    }


    override fun onTouchEvent(event: MotionEvent?): Boolean {
        listener?.onClick()
        invalidate()
        Log.i(javaClass.name, "HAce click")
        return super.onTouchEvent(event)
    }

    fun setOnFaceClickListener(listener : OnFaceClickListener){
        this.listener = listener
    }

    interface OnFaceClickListener{
        fun onClick()
    }


}