package com.example.android.sampleexercise

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import com.example.android.sampleexercise.view.CartaView

class MainActivity : AppCompatActivity() {

    private var carta1 : CartaView? = null
    private var carta2 : CartaView? = null
    private var carta3 : CartaView? = null
    private var carta4 : CartaView? = null

    var cont: Int = 0
    var num1: Int = 0
    var num2: Int = 0
    var num3: Int = 0
    var num4: Int = 0
    var num5: Int = 0
    var num6: Int = 0
    var num7: Int = 0
    var num8: Int = 0

    var nuevo: Boolean = true
    var status: Boolean = false
    var resultTextView: TextView? = null
    var quedar: Button? = null
    var reiniciar:Button? =null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        findCarts()
        this.resultTextView = findViewById(R.id.texto)
        this.quedar = findViewById(R.id.quedar)
        this.reiniciar = findViewById(R.id.reiniciar)
    }

    fun randito (){
        if(nuevo){
            num1 = (1..13).random()
            num2 = (1..13).random()
            num3 = (1..13).random()
            num4 = (1..13).random()
            num5 = (1..13).random()
            num6 = (1..13).random()
            num7 = (1..13).random()
            num8 = (1..13).random()
        }else{

            nuevo = false
        }
    }

    //Trigger para recibir click de la fucion limpiar del textView
    // fun limpiarOnClick(view: View) {
    //instanciar variable
    //    var click: CartaView = view as CartaView

    //   click.setOnClickListener {
    //        JugadaJ()

    //    }
    //}

    fun JugadaJ(){
        //Voltear carta del jugador Onclick
        //   carta = numero
        cont++
    }

    fun JugadaM(){
        // Voltear carta de la mesa
    }

    fun reiniciar(){
        //todas las cartas regresan al estado inicial;
        nuevo = true
    }

    fun ganador(){
        // Quien gana la partida

        //Total del jugador por cada jugada
        var totalJ1 : Int = 0

        //Total de la mesa por cada jugada
        var totalM1 : Int = 0

        if(cont == 3){
            totalJ1 = num1 + num2
            totalM1 = num5 + num6
        }else if (cont == 4){
            totalJ1 = num1 + num2 + num3
            totalM1 = num5 + num6 + num7
        }else if (cont == 5){
            totalJ1 = num1 + num2
            totalM1 = num5 + num6
        }else if (cont == 6){
            totalJ1 = num1 + num2
            totalM1 = num5 + num6
        }else if (cont == 7){
            totalJ1 = num1 + num2
            totalM1 = num5 + num6
        }else if (cont == 8){
            totalJ1 = num1 + num2
            totalM1 = num5 + num6
        }

        if(totalM1 >= 21){
            status = true
            resultTextView?.text = "La mesa gana"
        }else if (totalJ1 >= 21){
            status = true
            resultTextView?.text = "Eljugador gana"
        }
    }

    private fun findCarts(){
        carta1 = findViewById(R.id.jugador1)
        carta2 = findViewById(R.id.jugador2)
        carta3 = findViewById(R.id.jugador3)
        carta4 = findViewById(R.id.jugador4)
    }
}