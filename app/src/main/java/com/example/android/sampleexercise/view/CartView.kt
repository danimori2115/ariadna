package com.example.android.sampleexercise.view

import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout
import com.example.android.sampleexercise.R

class CartView : FrameLayout {

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        initView()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        initView()
    }

    constructor(context: Context) : super(context) {
        initView()
    }

    private fun initView() {
        val view = inflate(context, R.layout.activity_main, null)
        addView(view)
    }
}